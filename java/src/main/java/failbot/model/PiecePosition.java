package failbot.model;

public class PiecePosition {
	public int lap;
	public int pieceIndex;
	public double inPieceDistance;

	public static class Lane {
		public int startLaneIndex;
		public int endLaneIndex;
	}
}
