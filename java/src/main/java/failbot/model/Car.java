package failbot.model;

public class Car {
	public CarId id;
	public Dimensions dimensions;

	public static class Dimensions {
		double length;
		double width;
		double guideFlagPosition;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Car [id=" + id + "]";
	}

}