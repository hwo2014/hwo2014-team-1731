package failbot.model;

public class RaceTime {
	public int lap;
	public int ticks;
	public int millis;

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RaceTime [lap=" + lap + ", ticks=" + ticks + ", millis=" + millis + "]";
	}
}
