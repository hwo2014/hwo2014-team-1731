package failbot.model;

import com.google.gson.annotations.SerializedName;

public class Piece {
	public double length;
	public double radius;
	public double angle;

	@SerializedName("switch")
	public boolean isSwitch;

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[len=" + length + ", rad=" + radius + ", ang=" + angle + ", sw=" + isSwitch + "]";
	}

}