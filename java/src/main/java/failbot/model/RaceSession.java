package failbot.model;

public class RaceSession {
	public int laps;
	public int maxLapTimeMs;
	public int durationMs;
	public boolean quickRace;
}