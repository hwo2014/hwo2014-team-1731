package failbot.model;

import java.util.List;

public class Track {
	public String id;
	public String name;

	public static class Lane {
		public int distanceFromCenter;
		public int index;
	}

	public List<Lane> lanes;
	public List<Piece> pieces;

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Track [id=" + id + ", name=" + name + ", pieces=" + pieces + "]";
	}
}
