package failbot.model;

import java.util.List;

public class RaceData {
	public Track track;
	public List<Car> cars;
	public RaceSession raceSession;
}