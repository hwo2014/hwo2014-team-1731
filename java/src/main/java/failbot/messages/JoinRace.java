package failbot.messages;

public class JoinRace extends SendMsg {
	public BotId botId;
	public String trackName;
	public int carCount;

	// public String password;

	public static class BotId {
		public String name;
		public String key;

		public BotId(String name, String key) {
			this.name = name;
			this.key = key;
		}
	}

	@Override
	public String msgType() {
		return "joinRace";
	}

	public JoinRace(String name, String key, String trackName, int carCount) {
		this.botId = new BotId(name, key);
		this.carCount = carCount;
		this.trackName = trackName;
	}
}
