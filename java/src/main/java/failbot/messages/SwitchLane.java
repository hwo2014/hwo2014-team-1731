package failbot.messages;

public class SwitchLane extends SendMsg {
	private String data;

	public SwitchLane(final String data) {
		this.data = data;
	}

	@Override
	public Object msgData() {
		return data;
	}

	@Override
	public String msgType() {
		return "switchLane";
	}
}