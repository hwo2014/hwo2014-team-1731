package failbot.messages;

import failbot.model.CarId;
import failbot.model.RaceTime;

public class LapFinished {
	public CarId car;
	public RaceTime raceTime;
	public RaceTime lapTime;
}
