package failbot.messages;

import java.util.List;

import failbot.model.CarPosition;

public class CarPositions {
	public String msgType;
	public String gameId;
	public int gameTick;

	public List<CarPosition> data;
}
