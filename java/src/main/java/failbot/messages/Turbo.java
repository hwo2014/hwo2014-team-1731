package failbot.messages;

public class Turbo extends SendMsg {
	private String data;

	public Turbo(final String data) {
		this.data = data;
	}

	@Override
	public Object msgData() {
		return data;
	}

	@Override
	public String msgType() {
		return "turbo";
	}
}