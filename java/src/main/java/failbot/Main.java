package failbot;

import java.io.IOException;

public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Failbot started!");
		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		new Connection(host, port, botName, botKey);
	}
}