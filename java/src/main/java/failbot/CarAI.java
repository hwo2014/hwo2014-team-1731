package failbot;

import java.util.List;

import failbot.messages.CarPositions;
import failbot.messages.GameInit;
import failbot.messages.LapFinished;
import failbot.messages.MsgWrapper;
import failbot.messages.TurboAvailable;
import failbot.model.CarId;
import failbot.model.CarPosition;
import failbot.model.Piece;
import failbot.model.PiecePosition;

public class CarAI {
	private final Connection connection;

	private CarId myCar;

	private List<Piece> pieces;

	private int currentLap = 0;
	private boolean crashed = false;

	private boolean turboActive = false;
	private boolean turboAvailable = false;
	private int turboTicksLeft = 0;

	public CarAI(final Connection connection) {
		this.connection = connection;
	}

	public void calculateSpeed() {

	}

	private int getNextIndex(int index) {
		return (index == pieces.size() - 1 ? 0 : index + 1);
	}

	public double getDistanceToCorner(CarPosition position) {
		PiecePosition piecePos = position.piecePosition;
		Piece piece = pieces.get(piecePos.pieceIndex);

		if (piece.angle != 0) {
			return 0;
		}

		double distance = piece.length - piecePos.inPieceDistance;

		for (int i = getNextIndex(piecePos.pieceIndex); i != piecePos.pieceIndex; i = getNextIndex(i)) {
			piece = pieces.get(i);

			if (piece.angle != 0) {
				break;
			}

			distance += piece.length;
		}

		return distance;
	}

	public void handleCarPositions(CarPositions positions) {
		if (crashed) {
			connection.sendPing();
			return;
		}

		CarPosition pos = null;

		for (CarPosition p : positions.data) {
			if (p.id.equals(myCar)) {
				pos = p;
				break;
			}
		}

		int currIndex = pos.piecePosition.pieceIndex;
		Piece piece = pieces.get(currIndex);

		double nextCorner = getDistanceToCorner(pos);

		if (nextCorner > 800 && turboAvailable) {
			turboAvailable = false;
			turboActive = true;
			connection.sendTurbo();
		}

		double angle = Math.abs(piece.angle);
		double slip = Math.abs(pos.angle);
		double throttle = 1;

		if (nextCorner > 140) {
			throttle = 1;
		} else {
			throttle = 1 * nextCorner / 250;
		}

		if (angle > 10) {
			throttle = 0.88 - (slip / 100);// - (angle/piece.radius)*0.3;
		}

		if (turboActive) {
			turboTicksLeft--;

			if (turboTicksLeft == 0) {
				turboActive = false;
			}

			throttle = 0.9 * Math.abs(nextCorner / 800);
		}

		System.out.print(piece + "[" + currIndex + "] lap:" + currentLap + " slip: " + (int) slip);
		System.out.print(" throttle:" + (int) (throttle * 100) + " next corner: " + (int) getDistanceToCorner(pos));
		System.out.println(" turbo: " + turboActive);

		connection.sendThrottle(throttle);
	}

	public void handleUnknown(MsgWrapper msg) {
		System.out.println("unknown message: " + msg.msgType + ":" + (msg.data != null ? msg.data.toString() : ""));
		connection.sendPing();
	}

	public void handleJoin() {
		System.out.println("Joined the race");
		connection.sendPing();
	}

	public void handleGameInit(GameInit init) {
		System.out.println("Cars:" + init.race.cars);
		System.out.println("Track: " + init.race.track);

		this.pieces = init.race.track.pieces;
		connection.sendPing();
	}

	public void handleYourCar(CarId car) {
		myCar = car;
	}

	public void handleLapFinished(LapFinished lapFinished) {
		int ms = lapFinished.lapTime.millis;
		System.out.format("** Laptime: %02d:%02d:%02d\n", (ms / 1000 / 60), ms / 1000, ms % 1000);

		currentLap++;
	}

	public void handleCrash(CarId car) {
		if (car.equals(myCar)) {
			System.out.println("Crash");
			crashed = true;
		}
	}

	public void handleSpawn(CarId car) {
		if (car.equals(myCar)) {
			System.out.println("Spawn");
			crashed = false;
		}
	}

	public void handleGameEnd(MsgWrapper fromJson) {
		System.out.println("Race finished");
	}

	public void handleGameStart(MsgWrapper fromJson) {
		System.out.println("Race started");
		connection.sendPing();
	}

	public void handleTurbo(TurboAvailable turbo) {
		System.out.println("Received turbo");
		turboAvailable = true;
		turboTicksLeft = (int) (turbo.turboDurationTicks * 2.2);
	}
}
