package failbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import failbot.messages.CarPositions;
import failbot.messages.GameInit;
import failbot.messages.Join;
import failbot.messages.JoinRace;
import failbot.messages.LapFinished;
import failbot.messages.MsgWrapper;
import failbot.messages.Ping;
import failbot.messages.SendMsg;
import failbot.messages.SwitchLane;
import failbot.messages.Throttle;
import failbot.messages.Turbo;
import failbot.messages.TurboAvailable;
import failbot.model.CarId;

public class Connection {
	final private Gson gson = new Gson();
	final private PrintWriter writer;
	private final Socket socket;

	public Connection(String host, int port, String botName, String botKey) throws UnknownHostException, IOException {
		CarAI ai = new CarAI(this);

		socket = new Socket(host, port);
		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		boolean custom = false;
		if (custom) {
			send(new JoinRace(botName, botKey, "keimola", 2));
		} else {
			send(new Join(botName, botKey));
		}

		String line = null;
		while ((line = reader.readLine()) != null) {
			JsonObject json = new JsonParser().parse(line).getAsJsonObject();
			String type = json.get("msgType").getAsString();

			if (type.equals("join")) {
				ai.handleJoin();
			} else if (type.equals("yourCar")) {
				ai.handleYourCar(gson.fromJson(json.get("data"), CarId.class));
			} else if (type.equals("gameInit")) {
				ai.handleGameInit(gson.fromJson(json.get("data"), GameInit.class));
			} else if (type.equals("gameEnd")) {
				ai.handleGameEnd(gson.fromJson(line, MsgWrapper.class));
			} else if (type.equals("gameStart")) {
				ai.handleGameStart(gson.fromJson(line, MsgWrapper.class));
			} else if (type.equals("lapFinished")) {
				ai.handleLapFinished(gson.fromJson(json.get("data"), LapFinished.class));
			} else if (type.equals("carPositions")) {
				ai.handleCarPositions(gson.fromJson(line, CarPositions.class));
			} else if (type.equals("crash")) {
				ai.handleCrash(gson.fromJson(json.get("data"), CarId.class));
			} else if (type.equals("spawn")) {
				ai.handleSpawn(gson.fromJson(json.get("data"), CarId.class));
			} else if (type.equals("turboAvailable")) {
				ai.handleTurbo(gson.fromJson(json.get("data"), TurboAvailable.class));
			} else {
				ai.handleUnknown(gson.fromJson(line, MsgWrapper.class));
			}

			if (socket.isClosed()) {
				return;
			}
		}

		socket.close();
	}

	private void send(final SendMsg msg) {
		// System.out.println("Sending:" + msg.toJson());

		writer.println(msg.toJson());
		writer.flush();
	}

	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendSwitchLane(String lane) {
		send(new SwitchLane(lane));
	}

	public void sendThrottle(double throttle) {
		send(new Throttle(throttle));
	}

	public void sendTurbo() {
		send(new Turbo("rawr"));
	}

	public void sendPing() {
		send(new Ping());
	}
}
